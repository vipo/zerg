package eu.homedir.zerg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import eu.homedir.zerg.api.Function1;
import eu.homedir.zerg.api.Function2;
import eu.homedir.zerg.api.Function3;
import eu.homedir.zerg.api.Function4;
import eu.homedir.zerg.api.Function5;
import eu.homedir.zerg.api.Function6;
import eu.homedir.zerg.api.Function7;

public class FunctionTests implements TestClasses{

	@Test
	public void function1() throws Exception{
		final D2 d2 = new D2("");
		final Function1<D1, D2> f = new Function1<D1, D2>() {
			public D2 apply(D1 arg) throws Exception {return d2;}
			public Class<D1> arg1Type() {return D1.class;}
			public Class<D2> resultType() {return D2.class;}
		};
		assertEquals(D2.class, f.resultType());
		assertEquals(D1.class, f.arg1Type());
		assertEquals(D1.class, f.argTypes().get(0));
		assertEquals(d2, f.apply(null));}

	@Test
	public void function2() throws Exception {
		final D3 d3 = new D3("");
		final Function2<D1, D2, D3> f = new Function2<D1, D2, D3>() {
			public D3 apply(D1 arg1, D2 arg2) throws Exception {return d3;}
			public Class<D2> arg2Type() {return D2.class;}
			public Class<D1> arg1Type() {return D1.class;}
			public Class<D3> resultType() {return D3.class;}
		};
		assertEquals(D3.class, f.resultType());
		assertEquals(D1.class, f.arg1Type());
		assertEquals(D2.class, f.arg2Type());
		assertEquals(D1.class, f.argTypes().get(0));
		assertEquals(D2.class, f.argTypes().get(1));
		assertNull(f.apply(null));
		assertEquals(d3, f.apply(null, null));
	}

	@Test
	public void function3() throws Exception{
		final D4 d4 = new D4("");
		final Function3<D1, D2, D3, D4> f = new Function3<D1, D2, D3, D4>() {
			public Class<D3> arg3Type() {return D3.class;}
			public Class<D2> arg2Type() {return D2.class;}
			public Class<D1> arg1Type() {return D1.class;}
			public Class<D4> resultType() {return D4.class;}
			public D4 apply(D1 arg1, D2 arg2, D3 arg3) throws Exception {
				return d4;
			}
		};
		assertEquals(D4.class, f.resultType());
		assertEquals(D1.class, f.arg1Type());
		assertEquals(D2.class, f.arg2Type());
		assertEquals(D3.class, f.arg3Type());
		assertEquals(D1.class, f.argTypes().get(0));
		assertEquals(D2.class, f.argTypes().get(1));
		assertEquals(D3.class, f.argTypes().get(2));

		assertNull(f.apply(null));
		assertNull(f.apply(null, null));
		assertEquals(d4, f.apply(null, null, null));
	}

	@Test
	public void function4() throws Exception{
		final D5 d5 = new D5("");
		final Function4<D1, D2, D3, D4, D5> f = new Function4<D1, D2, D3, D4, D5>() {
			public Class<D4> arg4Type() {return D4.class;}
			public Class<D3> arg3Type() {return D3.class;}
			public Class<D2> arg2Type() {return D2.class;}
			public Class<D1> arg1Type() {return D1.class;}
			public Class<D5> resultType() {return D5.class;}
			public D5 apply(D1 arg1, D2 arg2, D3 arg3, D4 arg4) throws Exception {
				return d5;
			}
		};
		assertEquals(D5.class, f.resultType());
		assertEquals(D1.class, f.arg1Type());
		assertEquals(D2.class, f.arg2Type());
		assertEquals(D3.class, f.arg3Type());
		assertEquals(D4.class, f.arg4Type());
		assertEquals(D1.class, f.argTypes().get(0));
		assertEquals(D2.class, f.argTypes().get(1));
		assertEquals(D3.class, f.argTypes().get(2));
		assertEquals(D4.class, f.argTypes().get(3));
		
		assertNull(f.apply(null));
		assertNull(f.apply(null, null));
		assertNull(f.apply(null, null, null));
		assertEquals(d5, f.apply(null, null, null, null));
	}

	@Test
	public void function5() throws Exception{
		final D6 d6 = new D6("");
		final Function5<D1, D2, D3, D4, D5, D6> f = new Function5<D1, D2, D3, D4, D5, D6>() {
			public Class<D5> arg5Type() {return D5.class;}
			public Class<D4> arg4Type() {return D4.class;}
			public Class<D3> arg3Type() {return D3.class;}
			public Class<D2> arg2Type() {return D2.class;}
			public Class<D1> arg1Type() {return D1.class;}
			public Class<D6> resultType() {return D6.class;}
			public D6 apply(D1 arg1, D2 arg2, D3 arg3, D4 arg4, D5 arg5) throws Exception {
				return d6;
			}
		};
		assertEquals(D6.class, f.resultType());
		assertEquals(D1.class, f.arg1Type());
		assertEquals(D2.class, f.arg2Type());
		assertEquals(D3.class, f.arg3Type());
		assertEquals(D4.class, f.arg4Type());
		assertEquals(D5.class, f.arg5Type());
		assertEquals(D1.class, f.argTypes().get(0));
		assertEquals(D2.class, f.argTypes().get(1));
		assertEquals(D3.class, f.argTypes().get(2));
		assertEquals(D4.class, f.argTypes().get(3));
		assertEquals(D5.class, f.argTypes().get(4));
		
		assertNull(f.apply(null));
		assertNull(f.apply(null, null));
		assertNull(f.apply(null, null, null));
		assertNull(f.apply(null, null, null, null));
		assertEquals(d6, f.apply(null, null, null, null, null));
	}
	
	@Test
	public void function6() throws Exception{
		final D7 d7 = new D7("");
		final Function6<D1, D2, D3, D4, D5, D6, D7> f = new Function6<D1, D2, D3, D4, D5, D6, D7>() {
			public Class<D6> arg6Type() {return D6.class;}
			public Class<D5> arg5Type() {return D5.class;}
			public Class<D4> arg4Type() {return D4.class;}
			public Class<D3> arg3Type() {return D3.class;}
			public Class<D2> arg2Type() {return D2.class;}
			public Class<D1> arg1Type() {return D1.class;}
			public Class<D7> resultType() {return D7.class;}
			public D7 apply(D1 arg1, D2 arg2, D3 arg3, D4 arg4, D5 arg5, D6 arg6) throws Exception {
				return d7;
			}
		};
		assertEquals(D7.class, f.resultType());
		assertEquals(D1.class, f.arg1Type());
		assertEquals(D2.class, f.arg2Type());
		assertEquals(D3.class, f.arg3Type());
		assertEquals(D4.class, f.arg4Type());
		assertEquals(D5.class, f.arg5Type());
		assertEquals(D6.class, f.arg6Type());
		assertEquals(D1.class, f.argTypes().get(0));
		assertEquals(D2.class, f.argTypes().get(1));
		assertEquals(D3.class, f.argTypes().get(2));
		assertEquals(D4.class, f.argTypes().get(3));
		assertEquals(D5.class, f.argTypes().get(4));
		assertEquals(D6.class, f.argTypes().get(5));

		
		assertNull(f.apply(null));
		assertNull(f.apply(null, null));
		assertNull(f.apply(null, null, null));
		assertNull(f.apply(null, null, null, null));
		assertNull(f.apply(null, null, null, null, null));
		assertEquals(d7, f.apply(null, null, null, null, null, null));
	}

	@Test
	public void function7() throws Exception{
		final D8 d8 = new D8("");
		final Function7<D1, D2, D3, D4, D5, D6, D7, D8> f = new Function7<D1, D2, D3, D4, D5, D6, D7, D8>() {
			public Class<D7> arg7Type() {return D7.class;}
			public Class<D6> arg6Type() {return D6.class;}
			public Class<D5> arg5Type() {return D5.class;}
			public Class<D4> arg4Type() {return D4.class;}
			public Class<D3> arg3Type() {return D3.class;}
			public Class<D2> arg2Type() {return D2.class;}
			public Class<D1> arg1Type() {return D1.class;}
			public Class<D8> resultType() {return D8.class;}
			public D8 apply(D1 arg1, D2 arg2, D3 arg3, D4 arg4, D5 arg5, D6 arg6, D7 arg7) throws Exception {
				return d8;
			}
		};
		assertEquals(D8.class, f.resultType());
		assertEquals(D1.class, f.arg1Type());
		assertEquals(D2.class, f.arg2Type());
		assertEquals(D3.class, f.arg3Type());
		assertEquals(D4.class, f.arg4Type());
		assertEquals(D5.class, f.arg5Type());
		assertEquals(D6.class, f.arg6Type());
		assertEquals(D7.class, f.arg7Type());
		assertEquals(D1.class, f.argTypes().get(0));
		assertEquals(D2.class, f.argTypes().get(1));
		assertEquals(D3.class, f.argTypes().get(2));
		assertEquals(D4.class, f.argTypes().get(3));
		assertEquals(D5.class, f.argTypes().get(4));
		assertEquals(D6.class, f.argTypes().get(5));
		assertEquals(D7.class, f.argTypes().get(6));
		
		assertNull(f.apply(null));
		assertNull(f.apply(null, null));
		assertNull(f.apply(null, null, null));
		assertNull(f.apply(null, null, null, null));
		assertNull(f.apply(null, null, null, null, null));
		assertNull(f.apply(null, null, null, null, null, null));
		assertEquals(d8, f.apply(null, null, null, null, null, null, null));
	}
}
