package eu.homedir.zerg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;

import org.junit.Test;

import eu.homedir.zerg.ExecutionException.Cause;
import eu.homedir.zerg.Executor;
import eu.homedir.zerg.api.Data;
import eu.homedir.zerg.api.Function1;

public class ExecutorTest implements TestClasses {

	private final D1 d1 = new D1("D1");
	private final D3 d3 = new D3("D3");
	private final D2 d2 = new D2("D2");
	private final D212 d212 = new D212("D212");
	
	private final Chain chain = Chain.create()
			.append(d1toD212)
			.append(d2toD3)
			.append(d1D3toD51)
			.append(d2D5toD6);
	
	@Test
	public void chainShouldKnowTypesItOperates(){	
		final Set<Class<? extends Data>> types = chain.types();
		assertTrue(types.contains(ValueData.class));
		assertTrue(types.contains(D1.class));
		assertTrue(types.contains(D2.class));
		assertTrue(types.contains(D21.class));
		assertTrue(types.contains(D212.class));
		assertTrue(types.contains(D3.class));
		assertTrue(types.contains(D5.class));
		assertTrue(types.contains(D51.class));
		assertTrue(types.contains(D6.class));
		assertEquals(9, types.size());
	}
	
	@Test
	public void executorShouldKnowTypesValues(){
		final Executor executor = chain.append(d1toD211).executor(d1, d3, d2, d212);
		assertTrue(d1 == executor.valueOf(D1.class));
		assertTrue(d212 == executor.valueOf(D2.class));
		assertTrue(d3 == executor.valueOf(D3.class));
		assertTrue(d212 == executor.valueOf(D21.class));
		assertTrue(d212 == executor.valueOf(D212.class));
		assertTrue(null == executor.valueOf(D211.class));
	}

	@Test
	public void simpleChainExecutionTest() throws Exception {
		final Executor executor = chain.executor(d1);
		final D6 result = executor.execute(D6.class);
		assertEquals("D2D5toD6(D1toD212(D1),D1D3toD51(D1,D2toD3(D1toD212(D1))))", result.value);
	}

	@Test
	public void simpleChainExecutionFailsWithNoValue() throws Exception {
		final Executor executor = chain.executor();
		try {
			executor.execute(D6.class);
			fail("There must be exception");
		} catch (ExecutionException ee){
			assertEquals(Cause.VALUE, ee.cause);
			assertEquals("No value available for " + D1.class, ee.getMessage());
		}
	}

	@Test
	public void functionFailsAreHandled() throws Exception {
		final String errorMessage = "Ooops :/";
		final Executor executor = Chain.create().append(new Function1<D1, D6>() {
			@Override
			public Class<D1> arg1Type() {
				return D1.class;
			}
			@Override
			public D6 apply(D1 arg) throws Exception {
				throw new RuntimeException(errorMessage);
			}
			@Override
			public Class<D6> resultType() {
				return D6.class;
			}
		}).executor(d1);
		try {
			executor.execute(D6.class);
			fail("There must be exception");
		} catch (ExecutionException ee){
			assertEquals(Cause.FUNCTION, ee.cause);
			assertEquals(errorMessage, ee.getCause().getMessage());
		}
	}

	@Test
	public void functionsAreNotAllowedToReturnNull() throws Exception {
		final Executor executor = Chain.create().append(new Function1<D1, D6>() {
			@Override
			public Class<D1> arg1Type() {
				return D1.class;
			}
			@Override
			public D6 apply(D1 arg) throws Exception {
				return null;
			}
			@Override
			public Class<D6> resultType() {
				return D6.class;
			}
		}).executor(d1);
		try {
			executor.execute(D6.class);
			fail("There must be exception");
		} catch (ExecutionException ee){
			assertEquals(Cause.VALUE, ee.cause);
			assertEquals("Function is not allowed to return null", ee.getMessage());
		}
	}

	
}
