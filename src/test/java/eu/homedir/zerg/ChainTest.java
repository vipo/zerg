package eu.homedir.zerg;

import org.junit.Assert;
import org.junit.Test;

import eu.homedir.zerg.api.Function0;

public class ChainTest implements TestClasses {
	
	@Test(expected=NullPointerException.class)
	public void functionCannotBeNull(){
		Chain.create().append((Function0<?>)null);
	}

	@Test(expected=NullPointerException.class)
	public void nestedChainCannotBeNull(){
		Chain.create().append((Chain)null);
	}
	
	@Test
	public void chainBuilderShouldBeReusable(){
		final Chain chain = Chain.create()
			.append(d1toD212);
		Assert.assertArrayEquals(new Object[]{d1toD212},
			chain.functions().toArray());
		Assert.assertArrayEquals(new Object[]{d1toD212,d2toD3},
			chain.append(d2toD3).functions().toArray());
		Assert.assertArrayEquals(new Object[]{d1toD212},
			chain.functions().toArray());
	}

	@Test
	public void nestedChainBuilderFlattensFunctionsCorrectly(){
		final Chain chain = Chain.create()
			.append(d1toD212)
			.append(Chain.create().append(d1D3toD51).append(d2toD3))
			.append(d2D5toD6);
		Assert.assertArrayEquals(new Object[]{d1toD212, d1D3toD51, d2toD3, d2D5toD6},
			chain.functions().toArray());
	}
	
}
