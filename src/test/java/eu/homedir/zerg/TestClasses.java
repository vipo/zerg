package eu.homedir.zerg;

import eu.homedir.zerg.api.Data;
import eu.homedir.zerg.api.Function1;
import eu.homedir.zerg.api.Function2;

public interface TestClasses {

	public static class ValueData implements Data {
		public final String value;
		public ValueData(String value){
			this.value = value;
		}
	}	
	public static class D1 extends ValueData {
		public D1(String value) {super(value);}
	}
	public static class D11 extends D1 {
		public D11(String value) {super(value);}
	};
	public static class D12 extends D1 {
		public D12(String value) {super(value);}
	};
	public static class D2 extends ValueData {
		public D2(String value) {super(value);}
	}
	public static class D21 extends D2 {
		public D21(String value) {super(value);}
	}
	public static class D211 extends D21 {
		public D211(String value) {super(value);}
	}
	public static class D212 extends D21 {
		public D212(String value) {super(value);}
	}
	public static class D3 extends ValueData {
		public D3(String value) {super(value);}
	}
	public static class D4 extends ValueData {
		public D4(String value) {super(value);}
	}	
	public static class D5 extends ValueData {
		public D5(String value) {super(value);}
	}
	public static class D51 extends D5 {
		public D51(String value) {super(value);}
	}
	public static class D6 extends ValueData {
		public D6(String value) {super(value);}
	}
	public static class D7 extends ValueData {
		public D7(String value) {super(value);}
	}
	public static class D8 extends ValueData {
		public D8(String value) {super(value);}
	}

	public static Function1<D1, D212> d1toD212 = 
		new Function1<D1, D212>() {
			public D212 apply(D1 arg) throws Exception {return new D212("D1toD212("+arg.value+")");}
			public Class<D212> resultType() {return D212.class;}
			public Class<D1> arg1Type() {return D1.class;}
		};
	public static Function1<D1, D211> d1toD211 =
		new Function1<D1, D211>() {
			public D211 apply(D1 arg) throws Exception {return new D211("D1toD211("+arg.value+")");}
			public Class<D211> resultType() {return D211.class;}
			public Class<D1> arg1Type() {return D1.class;}
		};
	public static Function1<D2, D3> d2toD3 = 
		new Function1<D2, D3>() {
			public D3 apply(D2 arg) throws Exception {return new D3("D2toD3("+arg.value+")");}
			public Class<D3> resultType() {return D3.class;}
			public Class<D2> arg1Type() {return D2.class;}
		};
	public static Function2<D1, D3, D51> d1D3toD51 =
		new Function2<D1, D3, D51>() {
			public D51 apply(D1 arg1, D3 arg2) throws Exception {return new D51("D1D3toD51("+arg1.value+","+arg2.value+")");}
			public Class<D51> resultType() {return D51.class;}
			public Class<D1> arg1Type() {return D1.class;}
			public Class<D3> arg2Type() {return D3.class;}
	};
	public static Function2<D2,D5,D6> d2D5toD6 =
		new Function2<D2, D5, D6>() {
			public D6 apply(D2 arg1, D5 arg2) throws Exception {return new D6("D2D5toD6("+arg1.value+","+arg2.value+")");}
			public Class<D6> resultType() {return D6.class;}
			public Class<D2> arg1Type() {return D2.class;}
			public Class<D5> arg2Type() {return D5.class;}
		};
}
