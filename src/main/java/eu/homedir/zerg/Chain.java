package eu.homedir.zerg;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.homedir.zerg.api.Data;
import eu.homedir.zerg.api.Function0;

public class Chain {

	private final List<Function0<?>> functions = new ArrayList<Function0<?>>();
	private final Set<Class<? extends Data>> types = new HashSet<Class<? extends Data>>();
	
	public static Chain create(){
		return new Chain();
	}
	
	private Chain(){}
	
	private Chain(final Function0<?> head, final Chain tail){
		this.functions.addAll(tail.functions);
		this.functions.add(head);
		final Collection<Class<? extends Data>> functionTypes = superTypes(head.resultType());
		for (final Class<? extends Data> clazz: head.argTypes()){
			functionTypes.addAll(superTypes(clazz));
		}
		//collect types
		this.types.addAll(tail.types);
		this.types.addAll(functionTypes);
	}
	
	public Chain append(final Function0<?> function){
		if (function == null) {
			throw new NullPointerException("Function cannot be null");
		}
		return new Chain(function, this);
	}
	
	public Chain append(final Chain chain){
		if (chain == null) {
			throw new NullPointerException("Chain cannot be null");
		}
		Chain result = this;
		for (final Function0<?> function: chain.functions()){
			result = new Chain(function, result);
		}
		return result;
	}
	
	public List<Function0<?>> functions(){
		return new ArrayList<Function0<?>>(functions);
	}
	
	public Set<Class<? extends Data>> types(){
		return new HashSet<Class<? extends Data>>(types);
	}
	
	@SuppressWarnings("unchecked")
	public List<Class<? extends Data>> superTypes(final Class<? extends Data> clazz){
		List<Class<? extends Data>> result = new ArrayList<Class<? extends Data>>();
		Class<?> curr = clazz;
		do {
			result.add((Class<? extends Data>)curr);
			curr = curr.getSuperclass();
		} while (Data.class.isAssignableFrom(curr));
		return result;
	}
	
	public Executor executor(Data... datas){
		return new Executor(this, datas);
	}
	
}