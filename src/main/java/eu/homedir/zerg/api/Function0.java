package eu.homedir.zerg.api;

import java.util.ArrayList;
import java.util.List;

public abstract class Function0<R extends Data> {
	
	public List<Class<? extends Data>> argTypes(){
		return new ArrayList<Class<? extends Data>>();
	}
	
	public abstract Class<R> resultType();

	public abstract R apply() throws Exception;
	
	public R applyArgs(final List<Data> args) throws Exception {
		return apply();
	}

}
