package eu.homedir.zerg.api;

import java.util.List;

import eu.homedir.zerg.Utils;

public abstract class Function2<A1 extends Data, A2 extends Data, R extends Data> 
	extends Function1<A1, R> {

	@Override
	public R apply(A1 arg) throws Exception{return null;}

	public abstract Class<A2> arg2Type();
	
	public abstract R apply(A1 arg1, A2 arg2) throws Exception;
	
	@Override
	public R applyArgs(final List<Data> args) throws Exception {
		return apply(
			arg1Type().cast(args.get(0)),
			arg2Type().cast(args.get(1)));
	}
	
	@Override
	public List<Class<? extends Data>> argTypes(){
		return Utils.appendToList(super.argTypes(), arg2Type());
	}
}
