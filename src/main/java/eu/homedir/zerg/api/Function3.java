package eu.homedir.zerg.api;

import java.util.List;

import eu.homedir.zerg.Utils;

public abstract class Function3<A1 extends Data, A2 extends Data, A3 extends Data, R extends Data>
	extends Function2<A1, A2, R> {

	public abstract Class<A3> arg3Type();
	
	@Override
	public R apply(A1 arg1, A2 arg2) throws Exception{return null;}
	
	public abstract R apply(A1 arg1, A2 arg2, A3 arg3) throws Exception;
	
	@Override
	public R applyArgs(final List<Data> args) throws Exception {
		return apply(
			arg1Type().cast(args.get(0)),
			arg2Type().cast(args.get(1)),
			arg3Type().cast(args.get(2)));
	}
	
	@Override
	public List<Class<? extends Data>> argTypes(){
		return Utils.appendToList(super.argTypes(), arg3Type());
	}
}
