package eu.homedir.zerg.api;

import java.util.List;

import eu.homedir.zerg.Utils;


public abstract class Function1<A extends Data, R extends Data> extends Function0<R> {
	
	@Override
	public R apply() throws Exception{return null;}
	
	public abstract Class<A> arg1Type();
	
	public abstract R apply(A arg) throws Exception;
	
	@Override
	public R applyArgs(final List<Data> args) throws Exception {
		return apply(arg1Type().cast(args.get(0)));
	}
	
	@Override
	public List<Class<? extends Data>> argTypes(){
		return Utils.appendToList(super.argTypes(), arg1Type());
	}

}
