package eu.homedir.zerg;

import java.util.ArrayList;
import java.util.List;

public abstract class Utils {

	public static <T> List<T> appendToList(List<T> list, T elem){
		final List<T> result = new ArrayList<T>();
		result.addAll(list);
		result.add(elem);
		return result;
	}
}
