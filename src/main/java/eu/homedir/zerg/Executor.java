package eu.homedir.zerg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.homedir.zerg.api.Data;
import eu.homedir.zerg.api.Function0;

public class Executor {
	
	private final Chain chain;
	private final Map<Class<? extends Data>, Data> state =
		new HashMap<Class<? extends Data>, Data>();
	
	Executor(final Chain chain, final Data[] datas){
		this.chain = chain;
		//prepare state for execution
		for (Data data: datas) {
			putStateAndPopulate(data.getClass(), data);
		}
	}

	public <R extends Data> R valueOf(final Class<R> type){
		return type.cast(state.get(type));
	}
	
	public <R extends Data> R execute(final Class<R> clazz) throws ExecutionException {
		for (final Function0<? extends Data> function: chain.functions()){
			final Class<? extends Data> functionResultClass = function.resultType();
			final List<Data> args = new ArrayList<Data>();
			for (final Class<? extends Data> functionArgClass: function.argTypes()){
				final Data value = state.get(functionArgClass);
				if (value == null){
					throw new ExecutionException("No value available for " + functionArgClass);
				}
				args.add(value);
			}
			Data value = null;
			try {
				value = function.applyArgs(args);
			} catch (Exception e){
				throw new ExecutionException(e);
			}
			if (value == null) {
				throw new ExecutionException("Function is not allowed to return null");
			}
			putStateAndPopulate(functionResultClass, value);
		}
		return clazz.cast(state.get(clazz));
	}

	private void putStateAndPopulate(final Class<? extends Data> type, final Data value){
		state.put(type, value);
		for (final Class<? extends Data> clazz: chain.superTypes(type)){
			state.put(clazz, value);
		}
	}

}
