package eu.homedir.zerg;

public class ExecutionException extends Exception {

	private static final long serialVersionUID = 42L;
	
	public static enum Cause {
		FUNCTION,
		VALUE;
	}
	
	public final Cause cause;
	
	public ExecutionException(String string) {
		super(string);
		this.cause = Cause.VALUE;
	}
	
	public ExecutionException(Throwable throwable) {
		super(throwable);
		this.cause = Cause.FUNCTION;
	}

}
